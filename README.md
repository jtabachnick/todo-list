# Prerequisites

You will need to have the following installed prior running the Todo List App

* Node.js
* NPM (Node Package Manager)
* Nodemon (for running the API)
    * `npm install -g nodemon` 


# Building and running the Front End

In the todo-list directory please open a terminal and run the following commands

### `npm install`
### `npm start`

Runs the app in the development mode.  
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Building and running the API

In the `src/backend` folder please open a terminal and run the following commands

### `npm install`
### `nodemon server`

# Database

The database is a remote MongoDB instance, so there is nothing that needs to be done to set it up.

# Interacting with the Todo List

View: [http://localhost:3000](http://localhost:3000)  
Edit/Delete: [http://localhost:3000/edit/{id}](http://localhost:3000/edit/5c8d3d6647a3558e70354d49)  
Create:   [http://localhost:3000/create](http://localhost:3000/create)