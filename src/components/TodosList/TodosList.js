import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import axios from 'axios';
import './TodosList.css'
import Todo from '../Todo/Todo'

export default class TodosList extends Component {
    constructor(props) {
        super(props);
        this.state = {todos: []};

        //counter for limiting the api calls on update
        this.updates = 0;

    }

    //get the todo list after the creation of the component
    componentDidMount() {
        this.getTodos();
    }

    /*
        update the component by getting the todos which will update the state
        the counter is to prevent an infinite loop of calls to the API
        as there would be a call to this method every time state is updated
        which is each time that getTodos is called. The 3 in the condition is used
        due to the delay in saving on the database side.
    */
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.updates < 3){
            this.getTodos();
            this.updates++;
        }

    }

    //gets the todo list from the api and updates state refreshing the component
    getTodos(){
        axios.get('http://localhost:4000/todos/')
            .then(response => {
                this.setState({ todos: response.data });
            })
            .catch(function (error){
                console.log(error);
            })
    }

    render() {
        const todos = [];

        this.state.todos.map(function(currentTodo, i){
            todos.push(<Todo todo={currentTodo} refresh={() => this.getTodos} key={i} />);
        })

        return (
            <div>
                <h3>Todos List</h3>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                        <th>Description</th>
                        <th>Responsible</th>
                        <th>Priority</th>
                        <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        { todos }
                    </tbody>
                </table>
            </div>
        )
    }
}
