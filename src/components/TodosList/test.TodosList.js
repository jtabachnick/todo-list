import React from 'react'
import { shallow } from 'enzyme'

import TodosList from './TodosList'

describe('TodosList', () => {
  let component, props

  beforeEach(() => {
    props = {}
    component = shallow(<TodosList {...props} />)
  })

  it('should', () => {
    expect(component).toMatchSnapshot()
  })
})