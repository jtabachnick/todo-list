import React from 'react'
import { shallow } from 'enzyme'

import CreateTodo from './CreateTodo'

describe('CreateTodo', () => {
  let component, props

  beforeEach(() => {
    props = {}
    component = shallow(<CreateTodo {...props} />)
  })

  it('should', () => {
    expect(component).toMatchSnapshot()
  })
})