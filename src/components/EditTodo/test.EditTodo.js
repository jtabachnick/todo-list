import React from 'react'
import { shallow } from 'enzyme'

import EditTodo from './EditTodo'

describe('EditTodo', () => {
  let component, props

  beforeEach(() => {
    props = {}
    component = shallow(<EditTodo {...props} />)
  })

  it('should', () => {
    expect(component).toMatchSnapshot()
  })
})