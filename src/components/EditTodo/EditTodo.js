import React, { Component } from 'react';
import axios from 'axios';

export default class EditTodo extends Component {
    constructor(props) {
        super(props);

        //binding the methods to allow for access to the state of the component
        this.onChangeTodoDescription = this.onChangeTodoDescription.bind(this);
        this.onChangeTodoResponsible = this.onChangeTodoResponsible.bind(this);
        this.onChangeTodoPriority = this.onChangeTodoPriority.bind(this);
        this.onChangeTodoCompleted = this.onChangeTodoCompleted.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.deleteTodo = this.deleteTodo.bind(this);

        //create the state object
        this.state = {
            todoDescription: '',
            todoResponsible: '',
            todoPriority: '',
            todoCompleted: false
        };
    }

    //load the todo after mounting to ensure that the values populate
    componentDidMount() {
       this.getTodo();
    }

    //get the todo to be edited
    getTodo(){
        axios.get('http://localhost:4000/todos/'+this.props.match.params.id)
            .then(response => {
                this.setState({
                    todoDescription: response.data.todoDescription,
                    todoResponsible: response.data.todoResponsible,
                    todoPriority: response.data.todoPriority,
                    todoCompleted: response.data.todoCompleted
                })
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    /*
        On change handlers.
        Save the changes to state so that they can be sent to the api
    */
    onChangeTodoDescription(e) {
        this.setState({
            todoDescription: e.target.value
        });
    }

    onChangeTodoResponsible(e) {
        this.setState({
            todoResponsible: e.target.value
        });
    }

    onChangeTodoPriority(e) {
        this.setState({
            todoPriority: e.target.value
        });
    }

    onChangeTodoCompleted(e) {
        this.setState({
            todoCompleted: !this.state.todoCompleted
        });
    }


    /*
        capture the click on the edit button
        prevent submission of the form
        create an object for saving
        call the api
    */
    onSubmit(e) {
        e.preventDefault();
        const obj = {
            todoDescription: this.state.todoDescription,
            todoResponsible: this.state.todoResponsible,
            todoPriority: this.state.todoPriority,
            todoCompleted: this.state.todoCompleted
        };

        axios.put('http://localhost:4000/todos/update/'+this.props.match.params.id, obj)
            .then(res => console.log(res.data)).then(
            this.props.history.push('/')
        );


    }

    //deletion method
    deleteTodo(){

        axios.delete('http://localhost:4000/todos/delete/' + this.props.match.params.id )
            .then(
                this.props.history.push('/')
            )
            .catch( function(error){
                console.log(error);
            })
    }

    render() {
        return (
            <div>
                <h3 align="center">Update Todo</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Description: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.todoDescription}
                                onChange={this.onChangeTodoDescription}
                        />
                    </div>
                    <div className="form-group">
                        <label>Responsible: </label>
                        <input
                            type="text"
                            className="form-control"
                            value={this.state.todoResponsible}
                            onChange={this.onChangeTodoResponsible}
                        />
                    </div>
                    <div className="form-group">
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input"
                                    type="radio"
                                    name="priorityOptions"
                                    id="priorityLow"
                                    value="Low"
                                    checked={this.state.todoPriority==='Low'}
                                    onChange={this.onChangeTodoPriority}
                            />
                            <label className="form-check-label">Low</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input"
                                    type="radio"
                                    name="priorityOptions"
                                    id="priorityMedium"
                                    value="Medium"
                                    checked={this.state.todoPriority==='Medium'}
                                    onChange={this.onChangeTodoPriority}
                            />
                            <label className="form-check-label">Medium</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input"
                                    type="radio"
                                    name="priorityOptions"
                                    id="priorityHigh"
                                    value="High"
                                    checked={this.state.todoPriority==='High'}
                                    onChange={this.onChangeTodoPriority}
                            />
                            <label className="form-check-label">High</label>
                        </div>
                    </div>
                    <div className="form-check">
                        <input  className="form-check-input"
                                id="completedCheckbox"
                                type="checkbox"
                                name="completedCheckbox"
                                onChange={this.onChangeTodoCompleted}
                                checked={this.state.todoCompleted}
                                value={this.state.todoCompleted}
                        />
                        <label className="form-check-label" htmlFor="completedCheckbox">
                            Completed
                        </label>
                    </div>

                    <br />

                    <div className="form-group">
                        <input type="submit" value="Update Todo" className="btn btn-primary" />
                        <button type="button" className="btn btn-primary ml-3" onClick={this.deleteTodo}>Delete</button>
                    </div>
                </form>
            </div>
        )
    }
}
