import React from 'react'
import { shallow } from 'enzyme'

import Todo from './Todo'

describe('Todo', () => {
  let component, props

  beforeEach(() => {
    props = {}
    component = shallow(<Todo {...props} />)
  })

  it('should', () => {
    expect(component).toMatchSnapshot()
  })
})