const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const todoRoutes = express.Router();
const PORT = 4000;

/*
    * setup of the database in MongoAtlas
    * done for simplicity, this should be abstracted into a an environment based setting
    * opening a connection with Mongoose plugin and creating a model for the collection
*/
const uri = "mongodb+srv://jtabachnick:7CcnvpMMwOZL4vWu@todolist-wyhi1.mongodb.net/todoLists?retryWrites=true";
mongoose.connect(uri, { useNewUrlParser: true });
const connection = mongoose.connection;
let Todo = require('./todo.model');

app.use(cors());
app.use(bodyParser.json());

//log to the console that the connection is ready
connection.once('open', function() {
    console.log("MongoDB database connection established successfully");
})


//get all todos
todoRoutes.route('/').get(function(req, res) {
    Todo.find(function(err, todos) {
        if (err) {
            console.log(err);
        } else {
            res.json(todos);
        }
    });
});

//get one todo
todoRoutes.route('/:id').get(function(req, res) {
    let id = req.params.id;
    Todo.findById(id, function(err, todo) {
        res.json(todo);
    });
});

//add a todo
todoRoutes.route('/add').post(function(req, res) {
    let todo = new Todo(req.body);
    todo.save()
        .then(todo => {
            res.status(200).json('todo added successfully');
        })
        .catch(err => {
            res.status(400).send('adding new todo failed');
        });
    return;
});

//update a todo
todoRoutes.route('/update/:id').put(function(req, res) {
    Todo.findById(req.params.id, function(err, todo) {
        if (!todo)
            res.status(404).send("data is not found");
        else
            todo.todoDescription = req.body.todoDescription;
            todo.todoResponsible = req.body.todoResponsible;
            todo.todoPriority = req.body.todoPriority;
            todo.todoCompleted = req.body.todoCompleted;
            todo.save().then(todo => {
            res.json('Todo updated!');
        })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
    });
});

//delete a todo
todoRoutes.route('/delete/:id').delete(function(req, res) {
    let id = req.params.id;
    Todo.deleteOne({_id : id}, function(err){
        if(err){
            console.log(err);
        }
    })
});


app.use('/todos', todoRoutes);

app.listen(PORT, function() {
    console.log("Server is running on Port: " + PORT);
});